var menuOpen = document.querySelector("#menu_trigger_open");
var menuClose = document.querySelector("#menu_trigger_close");
var menu = document.querySelector("#mobile_menu");

menuOpen.addEventListener("click", toggleMenu);
menuClose.addEventListener("click", closeMenu);

function toggleMenu() {
  menu.classList.toggle("active");
  if (menu.classList.contains("active")) {
    document.body.style.overflow = "hidden";
  } else {
    document.body.style.overflow = "auto";
  }
}

function closeMenu() {
  menu.classList.remove("active");
  document.body.style.overflow = "auto";
  submenu.classList.remove("active");
}

var submenuTrigger = document.querySelector("#submenu_trigger");
var submenu = document.querySelector("#submenu");

submenuTrigger.addEventListener("click", function () {
  submenu.classList.toggle("active");
});

var toStart = document.querySelector("#to_start");

toStart.addEventListener("click", function () {
  window.scrollTo({
    top: 0,
    behavior: "smooth",
  });
});

/* ------------------------------------------------- */

var openPopupButtons = document.querySelectorAll(".popup_open");


for (var i = 0; i < openPopupButtons.length; i++) {
  openPopupButtons[i].addEventListener("click", openPopup);
}

var popup = document.querySelector("#popup");
var popupOverlay = document.querySelector("#popup_overlay");
var buttonClosePopup = document.querySelector("#button_close");

buttonClosePopup.addEventListener("click", closePopup);

function openPopup() {
  popup.classList.add("active");
  popupOverlay.classList.add("active");
  document.body.style.overflow = "hidden";
}

function closePopup() {
  popup.classList.remove("active");
  popupOverlay.classList.remove("active");
  document.body.style.overflow = "auto";
}

/* ------------------------------------------------- */

$(document).ready(function () {
  $(".phone_mask").inputmask("+7(999)999-99-99");
});

/* ------------------------------------------------- */

$(".s3_slider.active").slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: true,
  nextArrow: document.querySelector("#s3_button_prev"),
  prevArrow: document.querySelector("#s3_button_next"),
  responsive: [
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
});

/* ------------------------------------------------- */

$(document).ready(function () {
  var s3Types = document.querySelectorAll(".s3 .types_item");

  for (var i = 0; i < s3Types.length; i++) {
    s3Types[i].addEventListener("click", changeType);
  }

  function changeType(e) {
    var currentActiveType = document.querySelector(".s3 .types_item.active");
    var currentActiveSlider = document.querySelector(".s3_slider.active");
    currentActiveType.classList.remove("active");
    currentActiveSlider.classList.remove("active");

    var clickedItemNumber = e.target.classList[1];

    var activeNowType = document.querySelector(
      ".s3 .types_item." + clickedItemNumber
    );
    var activeNowSlider = document.querySelector(
      ".s3_slider." + clickedItemNumber
    );

    activeNowType.classList.add("active");
    activeNowSlider.classList.add("active");

    $(activeNowSlider).slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      arrows: true,
      nextArrow: document.querySelector("#s3_button_prev"),
      prevArrow: document.querySelector("#s3_button_next"),
      responsive: [
        {
          breakpoint: 900,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });
  }
});

/* ------------------------------------------------- */

$(document).ready(function () {
  $("#s2_slider").slick({
    dots: true,
    arrows: true,
    nextArrow: document.querySelector("#s2_button_prev"),
    prevArrow: document.querySelector("#s2_button_next"),
  });
});

/* ------------------------------------------------- */

var nameInputs = $(".name_input");

for (var i = 0; i < nameInputs.length; i++) {
  nameInputs[i].addEventListener("input", nameInput);
}

function nameInput(event) {
  var namePhraseArr = event.target.value.split("");
  for (var i = 0; i < namePhraseArr.length; i++) {
    if (!isNaN(namePhraseArr[i])) {
      namePhraseArr[i] = "";
      event.target.value = namePhraseArr.join("");
    }
  }
}

/* ------------------------------------------------- */

var successPopupButtonClose = $("#success_button_close");

successPopupButtonClose.on("click", function () {
  document.getElementById("popup_success").classList.remove("active");
  document.body.style.overflow = "auto";
});

/* ------------------------------------------------- */

var questionsTriggers = document.querySelectorAll(".question_trigger");

for (var i = 0; i < questionsTriggers.length; i++) {
  questionsTriggers[i].addEventListener("click", showQuestion);
}

function showQuestion(e) {
  var activeNow = document.querySelector(".question_trigger.active");
  activeNow.classList.remove("active");
  e.target.classList.add("active");
}

/* ------------------------------------------------- */

$(document).ready(function () {
  var headerTypes = document.querySelectorAll(".header .types_item");
  var headerCircles = document.querySelectorAll(".header_circle");

  for (var i = 0; i < headerTypes.length; i++) {
    headerTypes[i].addEventListener("click", changeType);
    headerCircles[i].addEventListener("click", changeType);
  }

  function changeType(e) {
    var currentActiveType = document.querySelector(
      ".header .types_item.active"
    );
    var currentActiveSubtitle = document.querySelector(
      ".header_subtitle.active"
    );
    var currentActiveCircle = document.querySelector(".header_circle.active");
    currentActiveType.classList.remove("active");
    currentActiveSubtitle.classList.remove("active");
    currentActiveCircle.classList.remove("active");

    var clickedItemNumber = e.target.classList[1];

    var activeNowType = document.querySelector(
      ".header .types_item." + clickedItemNumber
    );
    var activeNowSubtitle = document.querySelector(
      ".header_subtitle." + clickedItemNumber
    );
    var activeNowCircle = document.querySelector(
      ".header_circle." + clickedItemNumber
    );

    activeNowType.classList.add("active");
    activeNowSubtitle.classList.add("active");
    activeNowCircle.classList.add("active");
  }
});

/* ------------------------------------------------- */
$(document).ready(function () {
  $("#s5_slider").slick({
    arrows: true,
    dots: true,
    nextArrow: document.querySelector("#s5_button_prev"),
    prevArrow: document.querySelector("#s5_button_next"),
  });
});

/* ------------------------------------------------- */

$(document).ready(function () {
  $("#s6_slider").slick({
    responsive: [
      {
        breakpoint: 9999,
        settings: "unslick",
      },
      {
        breakpoint: 700,
        settings: {
          arrows: true,
          nextArrow: document.querySelector("#s6_button_prev"),
          prevArrow: document.querySelector("#s6_button_next"),
        },
      },
    ],
  });
});

/* ------------------------------------------------- */

var successPopupButtonClose = $("#success_button_close");

successPopupButtonClose.on("click", function () {
  document.getElementById("popup_success").classList.remove("active");
  document.body.style.overflow = "auto";
});
$(document).ready(function () {
  $("#s3_slider").slick({
    arrows: true,
    nextArrow: document.querySelector("#s3_button_prev"),
    prevArrow: document.querySelector("#s3_button_next"),
  });
});

$("#s3_slider").slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: true,
  nextArrow: document.querySelector("#s3_button_prev"),
  prevArrow: document.querySelector("#s3_button_next"),
  responsive: [
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
});
/* ------------------------------------------------- */

var questionsTriggers = document.querySelectorAll(".question_trigger");

for (var i = 0; i < questionsTriggers.length; i++) {
  questionsTriggers[i].addEventListener("click", showQuestion);
}

function showQuestion(e) {
  var activeNow = document.querySelector(".question_trigger.active");
  activeNow.classList.remove("active");
  e.target.classList.add("active");
}

/* ------------------------------------------------- */
