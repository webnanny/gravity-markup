$(document).ready(function () {
  var btnSubmitFooter = document.getElementById("btn_submit_footer");
  var popupBtnSubmit = document.getElementById("btn_submit_popup");

  $(".js_checkbox_footer").on("change", function () {
    if (!$(".js_checkbox_footer").is(":checked")) {
      $("#btn_submit_footer").attr("disabled", true);
    } else {
      $("#btn_submit_footer").attr("disabled", false);
    }
  });

  $(".js_checkbox_popup").on("change", function () {
    if ($(".js_checkbox_popup").is(":checked")) {
      $("#btn_submit_popup").attr("disabled", false);
    } else {
      $("#btn_submit_popup").attr("disabled", true);
    }
  });

  btnSubmitFooter.addEventListener("click", function (e) {
    e.preventDefault();
    var name = $("#name_footer").val();
    var phone = $("#phone_footer").val();
    var date = $("#date_footer").val();
    var time = $("#time_footer").val();
    var type = $("#type_footer").val();
    var visit;
    if (window.innerWidth > 1200) {
      visit = $("#visit_footer_desktop").val();
    } else {
      visit = $("#visit_footer_mobile").val();
    }
    $.ajax({
      url: "../forms/form_footer.php",
      type: "post",
      data: {
        name: name,
        phone: phone,
        date: date,
        time: time,
        type: type,
        visit: visit,
      },
      error: function () {
        document.getElementById("popup_success").classList.add("active");
        $("#popup_success_title").html("Ой!");
        $("#note").html("Произошла ошибка!");
        document.body.style.overflow = "hidden";
      },
      beforeSend: function () {
        document.getElementById("popup_success").classList.add("active");
        $("#note").html("Отправляем данные...");
        document.body.style.overflow = "hidden";
      },
      success: function (result) {
        document.getElementById("popup_success").classList.add("active");
        document.body.style.overflow = "hidden";
        $("#note").html(result);
        $("#name").val("");
        $("#phone").val("");
        $("#time").val("");
        $("#date").val("");
      },
    });
  });

  popupBtnSubmit.addEventListener("click", function (e) {
    e.preventDefault();
    var name = $("#name_popup").val();
    var phone = $("#phone_popup").val();
    $.ajax({
      url: "../form_popup.php",
      type: "post",
      data: {
        name: name,
        phone: phone,
      },
      error: function () {
        document.getElementById("popup").classList.remove("active");
        document.getElementById("popup_success").classList.add("active");
        $("#popup_success_title").html("Ой!");
        $("#note").html("Произошла ошибка!");
        document.body.style.overflow = "hidden";
      },
      beforeSend: function () {
        document.getElementById("popup").classList.remove("active");
        document.getElementById("popup_success").classList.add("active");
        $("#note").html("Отправляем данные...");
        document.body.style.overflow = "hidden";
      },
      success: function (result) {
        document.getElementById("popup").classList.remove("active");
        document.getElementById("popup_success").classList.add("active");
        document.body.style.overflow = "hidden";
        $("#note").html(result);
        $("#name").val("");
        $("#phone").val("");
      },
    });
  });
});
btnSubmitFooter.addEventListener("click", function (e) {
  e.preventDefault();
  var name = $("#name_footer").val();
  var phone = $("#phone_footer").val();
  var time = $("#time").val();
  var date = $("#date").val();
  var select = $("#select").val();
  $.ajax({
    url: "../forms/form_footer.php",
    type: "post",
    data: {
      name: name,
      phone: phone,
      time: time,
      date: date,
      select: select,
    },
    error: function () {
      document.getElementById("popup_success").classList.add("active");
      $("#popup_success_title").html("Ой!");
      $("#note").html("Произошла ошибка!");
      document.body.style.overflow = "hidden";
    },
    beforeSend: function () {
      document.getElementById("popup_success").classList.add("active");
      $("#note").html("Отправляем данные...");
      document.body.style.overflow = "hidden";
    },
    success: function (result) {
      document.getElementById("popup_success").classList.add("active");
      document.body.style.overflow = "hidden";
      $("#note").html(result);
      $("#name").val("");
      $("#phone").val("");
      $("#time").val("");
      $("#date").val("");
    },
  });
});

popupBtnSubmit.addEventListener("click", function (e) {
  e.preventDefault();
  var name = $("#name_popup").val();
  var phone = $("#phone_popup").val();
  $.ajax({
    url: "../form_popup.php",
    type: "post",
    data: {
      name: name,
      phone: phone,
    },
    error: function () {
      document.getElementById("popup").classList.remove("active");
      document.getElementById("popup_success").classList.add("active");
      $("#popup_success_title").html("Ой!");
      $("#note").html("Произошла ошибка!");
      document.body.style.overflow = "hidden";
    },
    beforeSend: function () {
      document.getElementById("popup").classList.remove("active");
      document.getElementById("popup_success").classList.add("active");
      $("#note").html("Отправляем данные...");
      document.body.style.overflow = "hidden";
    },
    success: function (result) {
      document.getElementById("popup").classList.remove("active");
      document.getElementById("popup_success").classList.add("active");
      document.body.style.overflow = "hidden";
      $("#note").html(result);
      $("#name").val("");
      $("#phone").val("");
    },
  });

  btnSubmitFooter.addEventListener("click", function (e) {
    e.preventDefault();
    var name = $("#name_footer").val();
    var phone = $("#phone_footer").val();
    var user_message = $("#message").val();
    $.ajax({
      url: "../form_footer.php",
      type: "post",
      data: {
        name: name,
        phone: phone,
        user_message: user_message,
      },
      error: function () {
        document.getElementById("popup_success").classList.add("active");
        $("#popup_success_title").html("Ой!");
        $("#note").html("Произошла ошибка!");
        document.body.style.overflow = "hidden";
      },
      beforeSend: function () {
        document.getElementById("popup_success").classList.add("active");
        $("#note").html("Отправляем данные...");
        document.body.style.overflow = "hidden";
      },
      success: function (result) {
        document.getElementById("popup_success").classList.add("active");
        document.body.style.overflow = "hidden";
        $("#note").html(result);
        $("#name").val("");
        $("#phone").val("");
        $("#time").val("");
        $("#date").val("");
      },
    });
  });

  popupBtnSubmit.addEventListener("click", function (e) {
    e.preventDefault();
    var name = $("#name_popup").val();
    var phone = $("#phone_popup").val();
    $.ajax({
      url: "../form_popup.php",
      type: "post",
      data: {
        name: name,
        phone: phone,
      },
      error: function () {
        document.getElementById("popup").classList.remove("active");
        document.getElementById("popup_success").classList.add("active");
        $("#popup_success_title").html("Ой!");
        $("#note").html("Произошла ошибка!");
        document.body.style.overflow = "hidden";
      },
      beforeSend: function () {
        document.getElementById("popup").classList.remove("active");
        document.getElementById("popup_success").classList.add("active");
        $("#note").html("Отправляем данные...");
        document.body.style.overflow = "hidden";
      },
      success: function (result) {
        document.getElementById("popup").classList.remove("active");
        document.getElementById("popup_success").classList.add("active");
        document.body.style.overflow = "hidden";
        $("#note").html(result);
        $("#name").val("");
        $("#phone").val("");
      },
    });
  });
  popupBtnSubmit.addEventListener("click", function (e) {
    e.preventDefault();
    var name = $("#name_popup").val();
    var phone = $("#phone_popup").val();
    $.ajax({
      url: "../forms/form_popup.php",
      type: "post",
      data: {
        name: name,
        phone: phone,
      },
      error: function () {
        document.getElementById("popup").classList.remove("active");
        document.getElementById("popup_success").classList.add("active");
        $("#popup_success_title").html("Ой!");
        $("#note").html("Произошла ошибка!");
        document.body.style.overflow = "hidden";
      },
      beforeSend: function () {
        document.getElementById("popup").classList.remove("active");
        document.getElementById("popup_success").classList.add("active");
        $("#note").html("Отправляем данные...");
        document.body.style.overflow = "hidden";
      },
      success: function (result) {
        document.getElementById("popup").classList.remove("active");
        document.getElementById("popup_success").classList.add("active");
        document.body.style.overflow = "hidden";
        $("#note").html(result);
        $("#name").val("");
        $("#phone").val("");
      },
    });
  });
});
